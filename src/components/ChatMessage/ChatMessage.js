import React from 'react';
import "./ChatMessage.css";

const ChatMessage = props => {
  return (
      <div className="ChatMessage">
        <div className="MessageInfoBlock">
          <h4 className="MessageAuthor">{props.author}</h4>
          <p className="MessageDate">{props.date}</p>
        </div>
        <div className="MessageTextBlock">
          <p className="MessageText">{props.text}</p>
        </div>
      </div>
  );
};

export default ChatMessage;