import React from 'react';
import moment from 'moment';
import ChatMessage from "../ChatMessage/ChatMessage";

const Chat = ({msgData}) => {
  const msgArray = msgData.map(msg => {
    const date = moment(msg.datetime).calendar();

    return (
        <ChatMessage
            key={msg._id}
            author={msg.author}
            date={date}
            text={msg.message}
        />
    );
  });

  return (
      <div className="ChatBlock">
        {msgArray}
      </div>
  );
};

export default Chat;