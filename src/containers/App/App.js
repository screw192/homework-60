import React, {useState, useEffect} from "react";
import axios from "axios";
import './App.css';
import Chat from "../../components/Chat/Chat";
import SendMessage from "../SendMessage/SendMessage";

const App = () => {
  const [chat, setChat] = useState([]);
  const [lastMsgDate, setLastMsgDate] = useState("");
  const url = "http://146.185.154.90:8000/messages";

  useEffect(() => {
    axios.get(url).then((response) => {
      const messages = response.data;
      const lastMsgDate = messages[messages.length - 1].datetime;

      setChat(messages);
      setLastMsgDate(lastMsgDate);
    });
  }, []);

  useEffect(() => {
    let interval;

    if(lastMsgDate) {
      interval = setInterval(() => {
        if (lastMsgDate !== "") {
          const lastMsgUrl = `${url}?datetime=${lastMsgDate}`;
          axios.get(lastMsgUrl).then((response) => {
            if (response.data.length > 0) {
              const newMessages = response.data;
              const newLastMsgDate = newMessages[newMessages.length - 1].datetime;
              const newChatData = chat.concat(newMessages);

              setChat(newChatData);
              setLastMsgDate(newLastMsgDate);
            }
          });
        }
      }, 5000);
    }
    return () => clearInterval(interval);
  }, [lastMsgDate, chat]);

  return (
      <div className="App">
        <Chat msgData={chat} />
        <SendMessage apiUrl={url} />
      </div>
  );
}

export default App;
