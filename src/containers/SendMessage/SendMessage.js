import React, {useState} from 'react';
import axios from "axios";
import "./SendMessage.css";

const SendMessage = ({apiUrl}) => {
  const [message, setMessage] = useState("");

  const inputChange = string => {
    setMessage(string);
  };

  const inputClear = () => {
    setMessage("");
  };

  const sendMessage = messageText => {
    const data = new URLSearchParams();
    data.set('message', messageText);
    data.set('author', 'Ехидный колобок');

    axios
        .post(apiUrl, data)
        .catch(error => {
          console.log(error);
        });
  };

  return (
      <div className="SendMessageBlock">
        <textarea
            className="MessageTextArea"
            style={{resize: "none"}}
            placeholder="Type your message here..."
            value={message}
            onChange={e => inputChange(e.target.value)}
        />
        <button
            className="MessageButton"
            type="button"
            onClick={() => {
              sendMessage(message);
              inputClear();
            }}
        >Send</button>
      </div>
  );
};

export default SendMessage;